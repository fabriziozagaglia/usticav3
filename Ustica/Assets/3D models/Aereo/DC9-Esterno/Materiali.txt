-----------------------------
.FBX
-----------------------------

Materiali:
Mat-DC9-Esterno-Fusoliera-DX 
-> DC9-Esterno-Fusoliera-DX-4096-COLOR.png
-> DC9-Esterno-Fusoliera-DX-4096-BUMP.png
-> DC9-Esterno-Fusoliera-DX-4096-NORMAL.png
-> DC9-Esterno-Fusoliera-DX-4096-SPECULAR.png

Mat-DC9-Esterno-Fusoliera-SX 
-> DC9-Esterno-Fusoliera-SX-4096-COLOR.png
-> DC9-Esterno-Fusoliera-DX-4096-BUMP.png
-> DC9-Esterno-Fusoliera-DX-4096-NORMAL.png
-> DC9-Esterno-Fusoliera-DX-4096-SPECULAR.png

Mat-DC9-Esterno-Luci  -> materiale rosso non necessita di texture
Mat-DC9-Esterno -> materiale bianco non necessita di texture

DC9-Esterno-MotoreDx.fbx -> DC9-Esterno-Fusoliera-DX.png
DC9-Esterno-MotoreSx.fbx -> DC9-Esterno-Fusoliera-SX.png

DC9-Esterno-MotoreTurbinaDx.fbx -> DC9-Esterno-Fusoliera-DX.png
DC9-Esterno-MotoreTurbinaSx.fbx -> DC9-Esterno-Fusoliera-SX.png

Textures:

NOTA: 
- le 2 texture COLOR sono simili, differenziandosi per le scritte che a destra e sinistra devono essere diverse a causa dello specchiamento delle UV.

- le texture BUMP, NORMAL e SPECULAR invece sono uguali per entrambi i lati perch� non coinvolgono le scritte.




