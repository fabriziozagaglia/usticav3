-----------
OGG026.FBX
-----------

Materiale -> Texture

Mat-OGG26-Legno          -> OGG26-COLOR.png
Mat-OGG26-ElasticoNero   -> no texture, RGB 23,23,23
Mat-OGG26-ElasticoGiallo -> no texture, RGB 184,188,72
Mat-OGG26-Pelle          -> no texture, RGB 199,84,49
