// Cinema Suite
using UnityEngine;

namespace CinemaDirector
{
    /// <summary>
    /// Event for loading a new level
    /// </summary>
    [CutsceneItem("Utility", "Load Level", CutsceneItemGenre.GlobalItem)]
    public class LoadLevelEvent : CinemaGlobalEvent
    {
        public enum LoadLevelArgument
        {
            ByIndex,
            ByName,
        }

        public enum LoadLevelType
        {
            Standard,
            Additive,
            Async,
            AdditiveAsync,
        }

        public LoadLevelArgument Argument = LoadLevelArgument.ByIndex;
        public LoadLevelType Type = LoadLevelType.Standard;

        // The index of the level to be loaded.
        public int Level = 0;

        // The name of the level to be loaded.
        public string LevelName;

        /// <summary>
        /// Trigger the level load. Only in Runtime.
        /// </summary>
        public override void Trigger()
        {
            if (Application.isPlaying)
            {
                if (Argument == LoadLevelArgument.ByIndex)
                {
                    if (Type == LoadLevelType.Standard)
                    {
                        Application.LoadLevel(Level);
                    }
                    else if (Type == LoadLevelType.Additive)
                    {
                        Application.LoadLevelAdditive(Level);
                    }
                    else if (Type == LoadLevelType.Async)
                    {
                        Application.LoadLevelAsync(Level);
                    }
                    else if (Type == LoadLevelType.AdditiveAsync)
                    {
                        Application.LoadLevelAdditiveAsync(Level);
                    }
                }
                else if(Argument == LoadLevelArgument.ByName)
                {
                    if (Type == LoadLevelType.Standard)
                    {
                        Application.LoadLevel(LevelName);
                    }
                    else if (Type == LoadLevelType.Additive)
                    {
                        Application.LoadLevelAdditive(LevelName);
                    }
                    else if (Type == LoadLevelType.Async)
                    {
                        Application.LoadLevelAsync(LevelName);
                    }
                    else if (Type == LoadLevelType.AdditiveAsync)
                    {
                        Application.LoadLevelAdditiveAsync(LevelName);
                    }
                }
            }
        }
    }
}