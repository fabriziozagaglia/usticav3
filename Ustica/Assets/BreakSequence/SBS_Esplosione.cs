﻿using UnityEngine;
using System.Collections;

public class SBS_Esplosione : SequenceBlockScript
{
    public Light explosionLight;
    public AnimationCurve explosionLightIntensity;
    public AnimationCurve explosionLightRange;

    public UnityStandardAssets.ImageEffects.SunShafts sunshaft;
    public UnityStandardAssets.ImageEffects.Tonemapping toneMapping;
    public UnityStandardAssets.ImageEffects.BloomOptimized bloom;
    public AnimationCurve bloomIntensityCurve;
    public AnimationCurve bloomThresholdCurve;
    public AnimationCurve tonemappingCurve;
    public AnimationCurve shaftDistCurve;
    public AnimationCurve shaftIntensity;
    public AnimationCurve shaftSize;




    protected override void Initialize()
    {
        base.Initialize();

        sunshaft.enabled = toneMapping.enabled = bloom.enabled = true;
    }

    public override void Exec(float time)
    {
        base.Exec(time);

        float selfTimeNormalized = selfTime / sequenceBlock.timeSpan.duration;
        explosionLight.range     = explosionLightRange.Evaluate(selfTimeNormalized);
        explosionLight.intensity = explosionLightIntensity.Evaluate(selfTimeNormalized);

        bloom.intensity = bloomIntensityCurve.Evaluate(selfTimeNormalized);
        bloom.threshold = bloomThresholdCurve.Evaluate(selfTimeNormalized);

        Keyframe[] keys = toneMapping.remapCurve.keys;
        keys[1].value = Mathf.Lerp(0.5f, 0.75f, tonemappingCurve.Evaluate(selfTimeNormalized));
        toneMapping.remapCurve.keys = keys;

        sunshaft.maxRadius = 1.0f - shaftDistCurve.Evaluate(selfTimeNormalized);
        sunshaft.sunShaftIntensity = shaftIntensity.Evaluate(selfTimeNormalized);
        sunshaft.sunShaftBlurRadius = shaftSize.Evaluate(selfTimeNormalized);
        

    }


}
