﻿using UnityEngine;
using System.Collections;

public class SBS_LightOff : SequenceBlockScript
{
    public UnityStandardAssets.ImageEffects.SunShafts sunshaft;
    public UnityStandardAssets.ImageEffects.Tonemapping toneMapping;
    public UnityStandardAssets.ImageEffects.BloomOptimized bloom;
    public AnimationCurve flickeringCurve;
    static public Color originalAmbientLight;

    public Color shaftColor;
    
    protected override void Initialize()
    {
        base.Initialize();
        originalAmbientLight = RenderSettings.ambientLight;
    }

    public override void Exec(float time)
    {
        base.Exec(time);
        float normalSelfTime = selfTime / sequenceBlock.timeSpan.duration;
        float darkness = flickeringCurve.Evaluate(normalSelfTime);

        RenderSettings.ambientLight = darkness<=0?Color.Lerp(originalAmbientLight,Color.black,normalSelfTime*8f):originalAmbientLight;
        RenderSettings.reflectionIntensity = darkness<=0?0.1f:1f;

        sunshaft.maxRadius = 3;
        sunshaft.sunShaftBlurRadius = 3.05f;
        sunshaft.sunShaftIntensity = 1.6f;
        sunshaft.sunColor = Color.Lerp(sunshaft.sunColor, shaftColor, Time.deltaTime);


        Keyframe[] keys = toneMapping.remapCurve.keys;
        Keyframe key = keys[1];
        key.value = Mathf.Lerp(key.value, 0.15f, Time.deltaTime);
        key.time = Mathf.Lerp(key.time, 0.3f, Time.deltaTime);
        keys[1] = key;
        toneMapping.remapCurve.keys = keys;

        bloom.threshold = Mathf.Lerp(bloom.threshold, 0.18f, Time.deltaTime * 10);
        bloom.intensity = Mathf.Lerp(bloom.intensity, 1.85f, Time.deltaTime * 20);
    }

}
