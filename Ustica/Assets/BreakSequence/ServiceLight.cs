﻿using UnityEngine;
using System.Collections;

public class ServiceLight : MonoBehaviour {
    public GameObject[] lights;
    public Projector[]  selfProjectors;
    private Vector3 selfProjectorPos = new Vector3(-1.551f,2.11f,-22.775f);

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


        foreach (GameObject light in lights)
        {
            if (UnityEngine.Random.Range(0, 100) < 20) light.SetActive(false);
            if (UnityEngine.Random.Range(0, 100) < 50) light.SetActive(!light.activeSelf);
        }
        foreach (Projector projector in selfProjectors)
        {
            projector.material.SetColor("_Color", Color.Lerp(Color.black, Color.white, UnityEngine.Random.Range(0f, 1f)));
        }

	}
}
