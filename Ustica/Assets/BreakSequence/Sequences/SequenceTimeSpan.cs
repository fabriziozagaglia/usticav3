﻿using UnityEngine;
using System.Collections.Generic;
using System;


[Serializable]
public class SequenceTimeSpan  {

    public float _startTime = 0;
    public float _endTime   = 0.5f;
    public float _duration  = 0.5f;

    public float startTime
    {
        get { return _startTime; }
        set { _startTime = value; }
    }
    public float endTime
    {
        get { return _endTime; }
        set { _endTime = value; }
    }
    public float duration
    {
        get { _duration = _endTime - _startTime;  return _duration; }
        set { _duration = value; _endTime = _startTime + _duration; }
    }
}
