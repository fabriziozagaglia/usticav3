﻿using UnityEngine;
using System.Collections.Generic;
using System;


[Serializable]
public class Sequence  {
    #region Properties
        public string name;
        public SequenceTimeSpan timeSpan = new SequenceTimeSpan();
        public List<SequenceBlock> blocks = new List<SequenceBlock>();
        
        [NonSerialized]
        private SequenceTracker _parentTracker;
    #endregion

    #region Methods 
        public Sequence(string _name,float timeStart=0,float duration=0.5f)
        {
            name = _name;
            timeSpan.startTime = timeStart;
            timeSpan.duration  = duration;
        }

        public void Initialize(SequenceTracker sequenceTracker)
        {
            _parentTracker = sequenceTracker;

            foreach(SequenceBlock block in blocks)
            {
                block.Initialize(this);
                if(block.sequenceBlockScript!=null) block.sequenceBlockScript.sequenceBlock=block;
            }
        } 
    
        public void Exec(float time)
        {

            foreach(SequenceBlock block in blocks)
            {
                if(time<block.timeSpan.startTime) continue;
                if (time > block.timeSpan.endTime && block.started) continue;
                block.Exec(time);
            }
        }  
    #endregion
}


[Serializable]
public class SequenceBlock
{
    #region Properties
        public  SequenceTimeSpan timeSpan;
        public  SequenceBlockScript sequenceBlockScript;
        [NonSerialized]
        private Sequence _parentSequence;

        public bool started = false;
    #endregion

    #region Methods
        public void Initialize(Sequence sequence)
        {
           _parentSequence = sequence;
        }

        public void Exec(float time)
        {
            if (sequenceBlockScript!= null) {
                if(!sequenceBlockScript.gameObject.activeSelf) sequenceBlockScript.gameObject.SetActive(true);
                sequenceBlockScript.Exec(time);
            }
            started = true;
        }
    #endregion
}





