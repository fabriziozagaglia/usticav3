﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;

[CustomEditor(typeof(SequenceTracker))]
[CanEditMultipleObjects]
public class EffectsGroupEditor : Editor
{
    #region Properties
        //main
        SequenceTracker sequenceTracker;
        //ui
        private Color softGray = new Color(0.95f, 0.95f, 0.95f, 0.95f);
        const   string EMPTY = "";
        private float maxGlobalTimeSpan=10; //10
        private float viewWidth = 0;
        private int   offsetY = 0;
        private int   offsetX = 20; //0
        //selection / drag references
        private Sequence      selectedSequence =null;
        private SequenceBlock selectedBlock =null;
        private float sampleTime = 0;
        private bool draggingSampleTime = false;
        const int BUTTONS_Y_OFFSETS = 6; //Unity pro button style has a little y gap than boxes
        private SequenceTimeSpan draggingTimeSpan;
        private float draggingTimeSpanOffset;
        private SequenceTimeSpan resizingTimeSpan;

        private float leftAreaWidth;
        //private float lastSequenceAreaHeight;
        private float lastEffectAreaY;
        
        private Material lineMaterial;

        private bool isInsideTimeLine= false;
    #endregion

    #region styles
        GUIStyle styleBIG;
        GUIStyle styleBIGL;
        private void InitStyles()
        {
            if (styleBIG == null)
            {
                styleBIG = new GUIStyle();
                styleBIG.fontSize = 20;
                styleBIG.normal.textColor = Color.gray*1.1f;
                styleBIG.alignment = TextAnchor.MiddleCenter;
            }
            if (styleBIGL == null)
            {
                styleBIGL = new GUIStyle();
                styleBIGL.fontSize = 18;
                styleBIGL.normal.textColor = Color.white;
                styleBIGL.alignment = TextAnchor.MiddleLeft;
            }

        }
    #endregion

    #region Methods
    public override void OnInspectorGUI()
        {

            //Editor setup
           // if (Application.isPlaying) return;
            InitStyles();
            Repaint();
            viewWidth = EditorGUIUtility.currentViewWidth - 100;
            SequenceTracker oldSequenceTracker = sequenceTracker;
            sequenceTracker = (SequenceTracker)target;
            if (oldSequenceTracker != sequenceTracker)
            {
                selectedSequence = null; draggingTimeSpan = resizingTimeSpan  = null;
                selectedBlock = null;
            }
            InitGroup(sequenceTracker);
            //------------
            if (Application.isPlaying)
            {
                sampleTime = sequenceTracker.currentTime;
            }

            //Draw TimeLine
                offsetY = 10;
                Rect startRect = GUILayoutUtility.GetRect(0, 20); startRect.x = 0;
                startRect.y += 5;
                DrawAnimatorTimeLine(sequenceTracker,startRect);
                isInsideTimeLine = InsideRect(startRect);
            //------------

            //Draw Sequences
                foreach (Sequence sequence in sequenceTracker.sequences){
                    Rect lineRect;
                    if (DrawSequenceTimeSpan(sequence, sequence.name, out lineRect,selectedSequence == sequence)) {
                            selectedSequence = sequence; selectedBlock = null;
                            return;
                    }
                    GUI.color = Color.grey;
                    if (GUI.Button(new Rect(viewWidth, lineRect.y+BUTTONS_Y_OFFSETS, leftAreaWidth, lineRect.height), "[ - ]")){
                            sequenceTracker.sequences.Remove(sequence);
                            return;
                    }
                    //offsetY += 1;
                }
            //--------------
            Rect rect = GUILayoutUtility.GetRect(0, 30);
            rect.y += 5;
            GUI.color = Color.black;
            //right panel
                //if(Event.current.type==EventType.Repaint) lastSequenceAreaHeight = rect.y;
                leftAreaWidth = EditorGUIUtility.currentViewWidth - viewWidth;

                GUI.Box(new Rect(viewWidth, startRect.y, leftAreaWidth, rect.y-startRect.y+5),"");
                GUI.color = Color.gray;
                if(GUI.Button(new Rect(viewWidth,rect.y+ BUTTONS_Y_OFFSETS, leftAreaWidth, 30), "[+]"))
                {
                    sequenceTracker.sequences.Add(new Sequence(EMPTY));
                }
            //--------------
            //sequencePanel
            if(selectedSequence!=null) {
                DrawSequencePanel(new Rect(rect.x, rect.y + 10, viewWidth - rect.x * 2, 250),selectedSequence);
            }
            GUILayoutUtility.GetRect(0, 50);

            //Mouse UP
            if(Event.current.type == EventType.mouseUp) { 
                draggingTimeSpan = resizingTimeSpan = null;
                draggingSampleTime = false;
            }
        }


        /// <summary>
        /// Initialize group default things...
        /// </summary>
        /// <param name="group"></param>
        private void InitGroup(SequenceTracker group)
        { 

            float defaultPresetSpanDuration = 10/3f;
            if (group.sequences.Count == 0)
            {
                group.sequences.Add(new Sequence("DEFAULT", defaultPresetSpanDuration*0, defaultPresetSpanDuration));
            }
        }
    #endregion

    #region Draw items
        /// <summary>
        /// Draw animator timeline
        /// </summary>
        /// <param name="sequenceTracker"></param>
        /// <param name="rect"></param>
        private void DrawAnimatorTimeLine(SequenceTracker sequenceTracker,Rect rect)
        {
            //Pan & Zoom
            bool insideRect  = InsideRect(rect);
            if (insideRect)
            {
               if(Event.current.type == EventType.ScrollWheel) maxGlobalTimeSpan += Mathf.Sign(Event.current.delta.y*0.1f); maxGlobalTimeSpan = Mathf.Clamp(maxGlobalTimeSpan, 1, 200);
               if (Event.current.type == EventType.MouseDrag && Event.current.button >= 1) offsetX += (int)Event.current.delta.x;
            };
            //------------

            GUI.Box(ClampRectView(new Rect(offsetX, rect.y, 100, rect.height)), ""+sampleTime); //animator ref 
            float sampleTimeInView = TimeLineToViewX(sampleTime);
            Rect sampleRect = new Rect(sampleTimeInView, rect.y, 10, rect.height);

            sampleRect.x += offsetX;
            if (InsideRect(sampleRect))
            {
                GUI.color = Color.red;
                if (MouseDown()) draggingSampleTime = true;
            }
            if (draggingSampleTime || (insideRect && Event.current.type==EventType.MouseDrag) )
            {
                sampleTime = MouseXToTimeLine();
            }
            GUI.Box(new Rect(offsetX+sampleTimeInView-3, rect.y, 6, rect.height), "|");
            DrawGrid(rect, sampleRect);
        }
        
        /// <summary>
        /// Draw timeline grid & sample line
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="sampleRect"></param>
        private void DrawGrid(Rect rect,Rect sampleRect)
        {
            if(Event.current.type!=EventType.repaint) return;
            rect.x+=offsetX;
            CreateLineMaterial();
            lineMaterial.SetPass(0);
            GL.PushMatrix();
            GL.Begin(GL.LINES);
            GL.Color(new Color(1, 1, 1, 0.5F));


            for (int i = 0; i <10; ++i)
            {

                float sec = TimeLineToViewX(i);
                GL.Vertex3(rect.x + sec, rect.y, 0);
                GL.Vertex3(rect.x + sec, rect.y + rect.height, 0);

                
            }

            GL.Color(new Color(1, 1, 1, 0.1F));
            for (int i = 0; i <100; ++i) // viewWidth / 10
            {

                float sec = TimeLineToViewX(i/10f);
                GL.Vertex3(rect.x + sec, rect.y, 0);
                GL.Vertex3(rect.x + sec, rect.y + rect.height, 0);
            }



            GL.Color(new Color(1,0, 0, 0.3F));   GL.Vertex3(sampleRect.x, rect.y, 0);   GL.Vertex3(sampleRect.x, rect.y + rect.height+140, 0);


            GL.End();
            GL.PopMatrix();

            GUI.color = Color.white;
            for (int i = 0; i < 10; ++i)
            {
                float sec = TimeLineToViewX(i);
                GUI.Label(new Rect(rect.x + sec, rect.y+5, 10, rect.height), "" + i);
            }
            
        }
        
        /// <summary>
        /// Draw generic time span
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <param name="label"></param>
        /// <param name="baseRect"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        private bool DrawSequenceTimeSpan(Sequence sequence,  string label, out Rect baseRect,bool selected = false)
        {
            SequenceTimeSpan timeSpan = sequence.timeSpan;
            bool result = false;
            //setup ---
            baseRect = GUILayoutUtility.GetRect(0, 18);//22
            baseRect.x += offsetX -12;
            Rect rect = new Rect(baseRect.x+ TimeLineToViewX(timeSpan.startTime), baseRect.y+ offsetY, TimeLineToViewX(timeSpan.duration), 17);//20
            rect = ClampRectView( rect);
            //resizing ---
            if (resizingTimeSpan == timeSpan){
                resizingTimeSpan.endTime = MouseXToTimeLine(-maxGlobalTimeSpan);
                label += " - END:" + resizingTimeSpan.endTime;
            }else{
            //dragging ---
                if (draggingTimeSpan == timeSpan){
                    //rect.x += 1; // Unity fix
                    float originalStartTime = draggingTimeSpan.startTime;
                    float keepDuration = draggingTimeSpan.duration;
                    draggingTimeSpan.startTime = MouseXToTimeLine() - draggingTimeSpanOffset;
                    //snap to sample time ---
                    if (Mathf.Abs(draggingTimeSpan.startTime - sampleTime) < 0.1)  draggingTimeSpan.startTime = sampleTime;
                    //------
                    draggingTimeSpan.duration  = keepDuration;
                    label += " - START:" + draggingTimeSpan.startTime;
                    
                }
            }
            //Draw resizable rects & liste drag ---
            GUI.color=selected?Color.yellow:Color.white;
            Rect resizeBTRect = new Rect(rect.x + rect.width-10, rect.y, 15, rect.height);
            if (InsideRect(resizeBTRect)){
                if (MouseDown())resizingTimeSpan = timeSpan;                
                GUI.Box(resizeBTRect, EMPTY);
                result = false;
            } else {
                if (InsideRect(rect)){
                    GUI.color = selected ? Color.red : Color.green;
                    if (MouseDown()){ 
                        draggingTimeSpan = timeSpan;
                        draggingTimeSpanOffset = MouseXToTimeLine() - timeSpan.startTime;
                        result = true;
                    }
                }
            }
        
            //main rect
            GUI.Box(rect, label);
            GUI.Box(rect, EMPTY);
            //sai rects
            GUI.Box(new Rect(rect.x,rect.y,1,rect.height), EMPTY); GUI.Box(new Rect(rect.x+rect.width, rect.y, 1, rect.height), "");
            //background rect
            GUI.color = softGray * 2; GUI.Box(new Rect(0, rect.y, viewWidth, rect.height), EMPTY);

            return result;
        }

        /// <summary>
        /// Draw Sequence Panel
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="sequence"></param>
        private void DrawSequencePanel(Rect rect, Sequence sequence)
        {
            sequence.name = GUI.TextField(new Rect(rect.x, rect.y, rect.width,25), sequence.name);


            //Draw Sequences
            foreach (SequenceBlock block in sequence.blocks)
            {
                Rect lineRect;
                if (DrawSequenceBlockTimeSpan(block, out lineRect, selectedBlock == block))
                {
                    selectedSequence = sequence;
                    return;
                }
                GUI.color = Color.grey;
                if (GUI.Button(new Rect(viewWidth, lineRect.y + BUTTONS_Y_OFFSETS, leftAreaWidth, lineRect.height), "[ - ]"))
                {
                    sequence.blocks.Remove(block);
                    return;
                }
            }

            //--------------
            Rect bottomRect = GUILayoutUtility.GetRect(0,40);
            GUI.color = Color.black;
            leftAreaWidth = EditorGUIUtility.currentViewWidth - viewWidth;

            if (selectedBlock != null)
            {
                GUI.Box(bottomRect,""+selectedBlock.sequenceBlockScript, styleBIGL);

                GUI.color = Color.white;
                if (selectedBlock.sequenceBlockScript != null)
                {
                    var editor = Editor.CreateEditor(selectedBlock.sequenceBlockScript);
                    editor.OnInspectorGUI();
                }
            }

            GUI.color = Color.gray;
            if (GUI.Button(new Rect(viewWidth, bottomRect.y + BUTTONS_Y_OFFSETS, leftAreaWidth, 30), "[+]"))
            {
                sequence.blocks.Add(new SequenceBlock());
            }


    }
    #endregion

    #region Sequence Panel
        /// <summary>
        /// Draw generic time span
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <param name="label"></param>
        /// <param name="baseRect"></param>
        /// <param name="selected"></param>
        /// <returns></returns> 
        private bool DrawSequenceBlockTimeSpan(SequenceBlock block, out Rect baseRect,bool selected)
        {
            SequenceTimeSpan timeSpan = block.timeSpan;
            bool result = false;
            //setup ---
            baseRect = GUILayoutUtility.GetRect(0, 18);//22
            baseRect.x += offsetX - 12;
            Rect rect = new Rect(baseRect.x + TimeLineToViewX(timeSpan.startTime), baseRect.y + offsetY, TimeLineToViewX(timeSpan.duration), 17);//20
            rect = ClampRectView(rect);
            //resizing ---
            if (resizingTimeSpan == timeSpan)
            {
                resizingTimeSpan.endTime = MouseXToTimeLine(-maxGlobalTimeSpan);
                //label += " - END:" + resizingTimeSpan.endTime;
            }
            else
            {
                //dragging ---
                if (draggingTimeSpan == timeSpan)
                {
                    //rect.x += 1; // Unity fix
                    float originalStartTime = draggingTimeSpan.startTime;
                    float keepDuration = draggingTimeSpan.duration;
                    draggingTimeSpan.startTime = MouseXToTimeLine() - draggingTimeSpanOffset;
                    //snap to sample time ---
                    if (Mathf.Abs(draggingTimeSpan.startTime - sampleTime) < 0.1) draggingTimeSpan.startTime = sampleTime;
                    //------
                    draggingTimeSpan.duration = keepDuration;
                   // label += " - START:" + draggingTimeSpan.startTime;

                }
            }
            //Draw resizable rects & liste drag ---
            GUI.color = selected ? Color.yellow : Color.white;
            Rect resizeBTRect = new Rect(rect.x + rect.width - 10, rect.y, 15, rect.height);
            if (InsideRect(resizeBTRect))
            {
                if (MouseDown()) resizingTimeSpan = timeSpan;
                GUI.Box(resizeBTRect, EMPTY);
                result = false;
            }
            else
            {
                if (InsideRect(rect))
                {
                    GUI.color = selected ? Color.red : Color.green;
                    if (MouseDown())
                    {
                        draggingTimeSpan = timeSpan;
                        draggingTimeSpanOffset = MouseXToTimeLine() - timeSpan.startTime;
                        selectedBlock= block;
                        result = true;
                    }
                }
            }

            //main rect
            //GUI.Box(rect, label);
            GUI.Box(rect, EMPTY);
            //sai rects
            GUI.Box(new Rect(rect.x, rect.y, 1, rect.height), EMPTY); GUI.Box(new Rect(rect.x + rect.width, rect.y, 1, rect.height), "");
            //background rect
            GUI.color = softGray * 2; GUI.Box(new Rect(0, rect.y, viewWidth, rect.height), EMPTY);
            
            block.sequenceBlockScript = (SequenceBlockScript)EditorGUI.ObjectField(rect, block.sequenceBlockScript, typeof(SequenceBlockScript));
            return result;
        }
    #endregion

    #region AUX
    /// <summary>
    /// Convert the mouse 
    /// </summary>
    /// <param name="extraOffset"></param>
    /// <returns></returns>
    private float MouseXToTimeLine(float extraOffset = 0)
        {
            return ViewXToTimeLine(Event.current.mousePosition.x, extraOffset);
        }

        /// <summary>
        /// Convert viewport x to timeline value
        /// </summary>
        /// <param name="viewX"></param>
        /// <param name="extraOffset"></param>
        /// <returns></returns>
        private float ViewXToTimeLine(float viewX,float extraOffset=0)
        {
            return (viewX - offsetX +extraOffset) / viewWidth * maxGlobalTimeSpan;
        }

        /// <summary>
        /// Convert timeline value to viewport X
        /// </summary>
        /// <param name="viewX"></param>
        /// <returns></returns>
        private float TimeLineToViewX(float viewX)
        {
            return viewX * viewWidth / maxGlobalTimeSpan;
        }

        /// <summary>
        /// Common mouse InsideRect
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        private bool InsideRect(Rect rect)
        {
            return (rect.Contains(Event.current.mousePosition));
        }

        /// <summary>
        /// Common bool MouseDown
        /// </summary>
        /// <returns></returns>
        private bool MouseDown()
        {
            return Event.current.type == EventType.MouseDown;
        }
        private bool MouseDownLeft() { return MouseDown() && Event.current.button  == 0; }
        private bool MouseDownRight() { return MouseDown() && Event.current.button == 1; }
        private bool MouseDownMiddle(){ return MouseDown() && Event.current.button == 2; } 

        /// <summary>
        /// Clamp rects width according to available viewport space
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        private Rect ClampRectView( Rect rect)
        {
            rect.x = Mathf.Clamp(rect.x, int.MinValue, viewWidth);
            rect.width = Mathf.Clamp(rect.width, 0, Mathf.Abs(viewWidth - rect.x));
            return rect;
        }

        /// <summary>
        /// Create a material to draw line.... :/
        /// </summary>
        private void CreateLineMaterial()
        {
            if (!lineMaterial)
            {
                var shader = Shader.Find("Hidden/Internal-Colored");
                lineMaterial = new Material(shader);
                lineMaterial.hideFlags = HideFlags.HideAndDontSave;
                lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
                lineMaterial.SetInt("_ZWrite", 0);
            }
        }
    #endregion

}

#region Reflection  / Types / Copy
    /// <summary>
    /// Reflections utils to populate GUI.popups
    /// </summary>
    public static class TypeUtils
    {
        public static List<Type> GetAllDerivedTypes(Type type)
        {
            return GetAllDerivedTypes(Assembly.GetAssembly(type),type);
        }

        public static List<Type> GetAllDerivedTypes(Assembly assembly, Type type)
        {
            return assembly
                .GetTypes()
                .Where(t => t != type && type.IsAssignableFrom(t))
                .ToList();
        }

        public static T CopyComponent<T>(T original, GameObject destination) where T : Component
        {
            System.Type type = original.GetType();
            Component copy = destination.AddComponent(type);
            System.Reflection.FieldInfo[] fields = type.GetFields();
            foreach (System.Reflection.FieldInfo field in fields)
            {
                switch (field.FieldType.Name)
                {
                    case "SequenceTimeSpan": {
                        SequenceTimeSpan originalTimeSpan = (SequenceTimeSpan)field.GetValue(original);
                        SequenceTimeSpan copyOfTimeSpan = new SequenceTimeSpan(); copyOfTimeSpan.startTime = originalTimeSpan.startTime; copyOfTimeSpan.duration = originalTimeSpan.duration;
                        field.SetValue(original,copyOfTimeSpan); break;
                    }
                    default: { field.SetValue(copy, field.GetValue(original)); break; }
                }
                
            }
            copy.hideFlags = HideFlags.HideInInspector;
            return copy as T;
        }
    }


#endregion