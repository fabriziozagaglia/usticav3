﻿using UnityEngine;
using System.Collections;

public class SequenceBlockScript : MonoBehaviour
{
    public SequenceBlock sequenceBlock;
    protected float globalTime;
    protected float selfTime {  get { return globalTime - sequenceBlock.timeSpan.startTime; } }

    protected bool _firstIterationDone = false;
    protected virtual void Initialize()
    {
        _firstIterationDone = true;
        this.gameObject.SetActive(true);
    }

    public virtual void Exec(float time)
    {
        if (!_firstIterationDone) Initialize();
        globalTime = time;
        //to override
    }
}
