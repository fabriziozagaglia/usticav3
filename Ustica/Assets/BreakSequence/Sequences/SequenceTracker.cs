﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SequenceTracker : MonoBehaviour {

    #region static definition properties
        public List<Sequence> sequences = new List<Sequence>();
    #endregion

    #region runtime properties
        protected float timeScale = 0.5f;                      //playback timescale
        protected float time = 0;                           //playback current time
        public float currentTime { get { return time; } }   //playback cuttert time public getter
        protected bool  exec = false;                       //executing
    #endregion


    #region Methods

        // Update is called once per frame
        void Update () {

            if (Input.GetKeyUp(KeyCode.Space))
            {
                exec= true;
            }

            if (exec)
            {
                time+=Time.deltaTime*timeScale;
                foreach(Sequence sequence in sequences)
                {
                    if(time<sequence.timeSpan.startTime) continue;
                    sequence.Exec(time);
                }
            }
        }

    #endregion

    #region EDITOR
        void OnEnable()
        {
            #if UNITY_EDITOR
            if (!Application.isPlaying) InitEditor();
            #endif

            if (Application.isPlaying)
            {
                foreach (Sequence sequence in sequences)
                {
                    sequence.Initialize(this);
                }
            }
        }

        private void InitEditor()
        {
            #if UNITY_EDITOR
             GameObject prefab = UnityEditor.PrefabUtility.GetPrefabParent(gameObject) as GameObject;
            #endif
        }

    #endregion
}
