﻿using UnityEngine;
using System.Collections;

public class SBS_DistaccoMotoreSX : SequenceBlockScript
{
    public GameObject motoreSX;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsController;

    protected override void Initialize()
    {
        base.Initialize();
        Rigidbody rigidbody = motoreSX.AddComponent<Rigidbody>();
        rigidbody.AddExplosionForce(-200, motoreSX.transform.position, 10);
        rigidbody.AddForce(Vector3.left*500+Vector3.back*100);
        rigidbody.AddTorque(new Vector3(-130, -130, -150));
    }

    public override void Exec(float time)
    {
        float eval = 10;
       // fpsController.extraRotation = new Vector3(6 * eval + Random.Range(-10, 10) * eval, 23f * eval + Random.Range(-10, 10) * eval, 335f * eval + Random.Range(-10, 10) * eval);
        base.Exec(time);
    }
}
