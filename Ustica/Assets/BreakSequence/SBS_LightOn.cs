﻿using UnityEngine;
using System.Collections;

public class SBS_LightOn : SequenceBlockScript
{
    public UnityStandardAssets.ImageEffects.SunShafts sunshaft;
    public UnityStandardAssets.ImageEffects.Tonemapping toneMapping;
    public UnityStandardAssets.ImageEffects.BloomOptimized bloom;
  
    public Color shaftColor;

    public AnimationCurve serviceLightsGlobal;
    public GameObject serviceLights;

    public override void Exec(float time)
    {
        base.Exec(time);

        bool light = serviceLightsGlobal.Evaluate(selfTime) > 1;
        RenderSettings.ambientLight = light?SBS_LightOff.originalAmbientLight*0.25f:Color.gray*0.4f;
        RenderSettings.reflectionIntensity = light ? 1 : 0;

        serviceLights.SetActive(light);

        /*
        RenderSettings.ambientLight = Color.black;
        RenderSettings.reflectionIntensity = 0.1f;

        sunshaft.maxRadius = 3;
        sunshaft.sunShaftBlurRadius = 3.05f;
        sunshaft.sunShaftIntensity = 1.6f;
        sunshaft.sunColor = Color.Lerp(sunshaft.sunColor, shaftColor, Time.deltaTime);


        Keyframe[] keys = toneMapping.remapCurve.keys;
        Keyframe key = keys[1];
        key.value = Mathf.Lerp(key.value, 0.15f, Time.deltaTime);
        key.time = Mathf.Lerp(key.time, 0.3f, Time.deltaTime);
        keys[1] = key;
        toneMapping.remapCurve.keys = keys;

        bloom.threshold = Mathf.Lerp(bloom.threshold, 0.18f, Time.deltaTime * 10);
        bloom.intensity = Mathf.Lerp(bloom.intensity, 1.85f, Time.deltaTime * 20);*/
    }

}
