﻿using UnityEngine;
using System.Collections;

public class SBS_DistaccoMotoreDX : SequenceBlockScript
{
    public GameObject motoreDX;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsController;

    protected override void Initialize()
    {
        base.Initialize();
        Rigidbody rigidbody =  motoreDX.AddComponent<Rigidbody>();
        rigidbody.AddExplosionForce(200, motoreDX.transform.position, 10);
        rigidbody.AddForce(Vector3.right*500+Vector3.back*100);
        rigidbody.AddTorque(new Vector3(130, 130, 150));
    }

    public override void Exec(float time)
    {
        float eval = 10;
       // fpsController.extraRotation = new Vector3(6 * eval + Random.Range(-10, 10) * eval, 23f * eval + Random.Range(-10, 10) * eval, 335f * eval + Random.Range(-10, 10) * eval);
        base.Exec(time);
    }
}
