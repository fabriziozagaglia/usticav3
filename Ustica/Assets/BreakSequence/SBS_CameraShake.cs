﻿using UnityEngine;
using System.Collections;

public class SBS_CameraShake : SequenceBlockScript
{

    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsController;
    public AnimationCurve cameraImpactForce;
    public UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration vignetting;
    public UnityStandardAssets.ImageEffects.CameraMotionBlur cameraBlur;
    public UnityStandardAssets.ImageEffects.MotionBlur motionBlur;

    public AnimationCurve rotX;
    public AnimationCurve rotY;
    public AnimationCurve rotZ;
    protected override void Initialize()
    {
        base.Initialize();
        vignetting.enabled = cameraBlur.enabled = motionBlur.enabled = true;
    }                

    public override void Exec(float time)
    {
        base.Exec(time);
    }

    public void FixedUpdate()
    {
        if (!_firstIterationDone) return;


        fpsController.applyExtraRotation = true;
        float selfTimeNormalized = selfTime / 1f; /// sequenceBlock.timeSpan.duration;
        float eval = cameraImpactForce.Evaluate(selfTimeNormalized);
        
        //fpsController.extraRotation = new Vector3(6 * eval + Random.Range(-10, 10) * eval, 23f * eval + Random.Range(-10, 10) * eval, 335f * eval + Random.Range(-10, 10) * eval);

        float impact = (eval + 0.5f);
        float rx = rotX.Evaluate(selfTime) ;
        float ry = rotY.Evaluate(selfTime) ;
        float rz = rotZ.Evaluate(selfTime) ;
        fpsController.extraRotation = new Vector3(rx, ry, rz) * impact + new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10)) * eval;

    }
}
