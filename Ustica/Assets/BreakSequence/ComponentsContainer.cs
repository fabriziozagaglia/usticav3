﻿using UnityEngine;
using System.Collections;
using System;


public class ComponentsContainer : SequenceBlockScript{

    public ComponentGroup[] componentGroup;

    public void Awake()
    {
        foreach(ComponentGroup group in componentGroup)
                group.Disable();
    }


    public void Enable(string groupName)
    {
        ComponentGroup group = GetGroup(groupName);
        if(group!= null) group.Enable();
    }

    public void Disable(string groupName)
    {
        ComponentGroup group = GetGroup(groupName);
        if (group != null) group.Disable();
    }

    public ComponentGroup GetGroup(string groupName)
    {
        foreach (ComponentGroup group in componentGroup)
        {
            if (group.name == groupName) return group;
        }
        return null;
    }

}


[Serializable]
public class ComponentGroup
{
    public string name;
    public MonoBehaviour[] components;

    public void Enable()
    {
        ToggleComponents(true);
    }

    public void Disable()
    {
        ToggleComponents(false);
    }

    private void ToggleComponents(bool toggle)
    {
        foreach (MonoBehaviour component in components)
        {
            component.enabled = toggle;
        }
    }
}