﻿using UnityEngine;
using System.Collections;

public class SBS_DC9MeshSwitch : SequenceBlockScript
{
    public GameObject standardDC9;
    public GameObject breakableDC9;

    protected override void Initialize()
    {
        base.Initialize();
        breakableDC9.SetActive(true);
        standardDC9.SetActive(false);
    }
}
