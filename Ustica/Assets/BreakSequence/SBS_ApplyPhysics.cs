﻿using UnityEngine;
using System.Collections;

public class SBS_ApplyPhysics : SequenceBlockScript
{
    public string gameObjectName;
    public float explosionForce = 100;
    public float explosionRange = 100;
    public Vector3 explosionPos = Vector3.down;
    public Vector3 forceDir = Vector3.right * 200;
    public Vector3 torque   = new Vector3(100, -100, 30);
    protected override void Initialize()
    {
        base.Initialize();
        Apply(GameObject.Find(gameObjectName));
    }

    private void Apply(GameObject go)
    {
        if (go == null) return;
        Rigidbody rigidbody = go.AddComponent<Rigidbody>();
        rigidbody.AddExplosionForce(explosionForce, explosionPos, explosionRange); //new Vector3(this.gameObject.transform.position * 10)
        rigidbody.AddForce(forceDir);
        rigidbody.AddTorque(torque);
        foreach (Transform child in go.transform) Apply(child.gameObject);
    }

    public override void Exec(float time)
    {

        base.Exec(time);
    }
}
