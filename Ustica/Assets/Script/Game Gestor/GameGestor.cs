﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections.Generic;

public enum GameplayState { start, destruction };

public enum GameState { playing, pause_menu };

public enum CameraUstica { insideDC9, outisideDC9, MIG, F104 };

public class GameGestor : MonoBehaviour {

    [SerializeField] private float TimeBeforeDestruction;
    float TempoPassato;

    public GameplayState actualGamePlayState;
    public GameState actualGameState;
    public CameraUstica actualCamera;

    public bool canChangeCameraDuringDestruction = false;

    [SerializeField] private PlayerGestor[] Players;
    int actualCameraId = 0;

    public List<CharactersManager> arrayPasseggeri = new List<CharactersManager>();
    Light[] lights;
    Projector[] projectors;
    GameObject[] objsLights;
    [SerializeField] private float MaxDistance_PassengersAnimaton;
    [SerializeField] private float MaxDistance_LightSwitch;
    [SerializeField] private float Passengers_optimize_every_X;
    [SerializeField] private float Lights_optimize_every_X;
    float timerOptimizerPassengers;
    float timerOptimizerLights;


    void Awake()
    {
        InitGestor();
    }
	
	// Update is called once per frame
	void Update ()
    {
        InputGestor();
        PassengersOptimizer();
        LightsOptimizer();
        //CheckTimeBeforeDestruction();
    }
    
    public void InitGestor()
    {
        actualGamePlayState = GameplayState.start;
        actualGameState = GameState.playing;
        //setting up cameras
        actualCamera = CameraUstica.insideDC9;
        actualCameraId = 0;
        if (Players != null)
        {
            for (int i = 0; i < Players.Length; i++)
            {
                if (Players[i].id != actualCameraId)
                    Players[i].SetActiveGameObject(false);
                else
                {
                    Players[i].SetActiveGameObject(true);
                    Debug.Log("-> Trovata e ativata la camera del PlayerGestor ID: " + actualCameraId);
                }
            }
        }
        objsLights = GameObject.FindGameObjectsWithTag("Lights");
        lights = new Light[objsLights.Length];
        projectors = new Projector[objsLights.Length];
        for (int i = 0; i < objsLights.Length; i++)
        {
            lights[i] = objsLights[i].GetComponentInChildren<Light>();            
            projectors[i] = objsLights[i].GetComponentInChildren<Projector>();
            if (lights[i]!=null)
                lights[i].enabled = false;
            if (projectors[i] != null)
                projectors[i].enabled = true;
        }
        Debug.Log("Numero Luci interne: " + lights.Length + ", Numero Projectors: " + projectors.Length);
        timerOptimizerLights = 0;
        timerOptimizerPassengers = 0;
    }

    private void InputGestor()
    {
        if (actualGameState == GameState.playing)
        {
            //switch della camera
            if (SwitchCameraGestor())
                return;
        }
    }

    int inputPrevValue = 0;
    private bool SwitchCameraGestor()
    {
        bool result = false;
        int inputValue = 0;
        //nel caso il cambio camera sia lockato durante la distruzione
        if (actualGamePlayState == GameplayState.destruction)
            if (!canChangeCameraDuringDestruction)
                return false;
        //ricevo il valore dall'asse switch camera       
        inputValue = (int)CrossPlatformInputManager.GetAxisRaw("SwitchCamera");
        if (inputValue != inputPrevValue)
        {
            inputPrevValue = inputValue;
            if (inputValue != 0)
            {                
                Debug.Log("Premuto il tasto switch Camera");
                if (Players.Length > 1)
                {
                    //esegue lo switch della camera in modo ricorsivo
                    result = CameraSwitcher(actualCameraId + inputValue, inputValue);
                }
            }
        }
        return result;
    }

    private bool CameraSwitcher(int nextCameraId, int input)
    {
        bool result = false;
        if (nextCameraId == actualCameraId)
           return false;
        else
        {
            if (Players.Length>nextCameraId && nextCameraId>=0)
            {
                if (Players[nextCameraId].isUnlocked)
                {
                    Players[actualCameraId].SetActiveGameObject(false);
                    Players[nextCameraId].SetActiveGameObject(true);
                    actualCameraId = nextCameraId;
                    actualCamera = Players[actualCameraId].thisCamera;
                    PassengersActivation();
                    result = true;
                    Debug.Log("-> Camera cambiata con camera id: " + nextCameraId);
                }
                else
                    result = CameraSwitcher(nextCameraId + input, input);
            }
            else
            {
                if (Players.Length <= nextCameraId)
                    result = CameraSwitcher(0, input);
                else
                    result = CameraSwitcher(Players.Length-1, input);
            }
        }
        return result;
    }

    private void CheckTimeBeforeDestruction()
    {
        if (actualGameState == GameState.playing)
        {
            if (TimeBeforeDestruction > TempoPassato)
                TempoPassato += Time.deltaTime;
            else
            {
                if (CheckPlayerPositionInsideArea())
                {
                    actualGamePlayState = GameplayState.destruction;

                    //VIUUUULLLLLLEEEEEENZZZZAAAAAAA
                }
            }
        }
    }

    private bool CheckPlayerPositionInsideArea()
    {
        bool result = false;

        return result;
    }

    #region Passeggeri
    private void PassengersOptimizer()
    {
        //Debug.Log(timerOptimizerPassengers);
        if (timerOptimizerPassengers > Passengers_optimize_every_X)
        {
            if (actualCamera == CameraUstica.insideDC9 || actualCamera == CameraUstica.outisideDC9)
            {
                for (int i = 0; i < arrayPasseggeri.Count; i++)                
                    arrayPasseggeri[i].AnimationOptimizer(Vector3.Distance(arrayPasseggeri[i].gameObject.transform.position, Players[actualCameraId].Player.transform.position)> MaxDistance_PassengersAnimaton);
            }
            timerOptimizerPassengers = 0;
        }
        else
        {
            timerOptimizerPassengers += Time.deltaTime;
        }
    }

    private void PassengersActivation()
    {
        if (actualCamera == CameraUstica.insideDC9 || actualCamera == CameraUstica.outisideDC9)
        {
            for (int i = 0; i < arrayPasseggeri.Count; i++)
                arrayPasseggeri[i].CharacterMesh.SetActive(true);
        }
        else
        {
            for (int i=0; i < arrayPasseggeri.Count; i++)
                arrayPasseggeri[i].CharacterMesh.SetActive(false);
        }
    }
    #endregion

    #region Luci
    private void LightsOptimizer()
    {        
        if (timerOptimizerLights > Lights_optimize_every_X)
        {
            if (actualCamera == CameraUstica.insideDC9)
            {
                for (int i = 0; i < objsLights.Length; i++)
                    SwitchLight(i);
            }
            timerOptimizerLights = 0;
        }
        else
        {
            timerOptimizerLights += Time.deltaTime;
        }
    }

    private void SwitchLight(int index)
    {
        if (Vector3.Distance(Players[actualCameraId].Player.transform.position, objsLights[index].gameObject.transform.position)
                           >= MaxDistance_LightSwitch)
        {
            if (lights[index] != null)            
                lights[index].enabled = false;
            if (projectors[index] != null)
                projectors[index].enabled = true;            
        }
        else
        {
            if (lights[index] != null)
            {
                lights[index].enabled = true;
                if (Vector3.Distance(Players[actualCameraId].Player.transform.position, objsLights[index].gameObject.transform.position)
                               >= MaxDistance_LightSwitch - 3)
                    lights[index].intensity = 0.6f;
                else
                    lights[index].intensity = 1f;
            }
            if (projectors[index] != null)
                projectors[index].enabled = false;            
        }
    }
    #endregion
}
