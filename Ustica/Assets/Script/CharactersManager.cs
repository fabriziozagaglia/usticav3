﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharactersManager : MonoBehaviour {

	public GameObject	CharacterMesh;

	public Texture[]	body;

	public Texture[]	cloth;
	public Texture[]	shirt;
	public Texture[]	shoes;
	public Texture[]	trousers;

	public AnimationClip[]	idles;

	public List<int>	currentBody;
	public List<int>	currentCloth;
	public List<int>	currentShirt;
	public List<int>	currentShoes;
	public List<int>	currentTrousers;

	private SkinnedMeshRenderer skr;
	private Animation 			anm;

	public void Setup(string code) {

	}

	public void InitAnimation() {

		anm = GetComponentInChildren<Animation>(); //GetComponent<Animation>();
		// anm.Stop();
		Invoke("startAnim", Random.Range(0f, 2f));
		InvokeRepeating("startAnim", Random.Range(3f, 4f), 40);
	}

	public void startAnim() {

		int i = Random.Range(0, idles.Length);

		//anm.Stop ();
		//anm.Play(idles[i].name);
		anm.CrossFade(idles[i].name, 0.5f);

	}

	public void Init() {
		string matname;		
		currentBody = new List<int>();
		currentCloth = new List<int>();
		currentShirt = new List<int>();
		currentShoes = new List<int>();
		currentTrousers = new List<int>();
		
		// Cerca i materiali e li associa
		skr = CharacterMesh.GetComponent<SkinnedMeshRenderer>();
        
		for (int i = 0; i < skr.sharedMaterials.Length; i++) {			
			//Debug.Log("Nome materiale " + skr.sharedMaterials[i].name);
            
			matname = skr.sharedMaterials[i].name;
			if (matname.IndexOf("Body") != -1) {
				currentBody.Add(i);
			} else if (matname.IndexOf("Shirt") != -1) {
				currentShirt.Add(i);
			} else if (matname.IndexOf("Jacket") != -1) {
				currentTrousers.Add(i);
			} else if (matname.IndexOf("Shoes") != -1) {
				currentShoes.Add(i);
			} else if (matname.IndexOf("Cloth") != -1) {
				currentCloth.Add(i);
			} else if (matname.IndexOf("Trousers") != -1) {
				currentTrousers.Add(i);
			} else if (matname.IndexOf("Dress") != -1) {
				currentCloth.Add(i);
			}            
		}
	}

	public void RandomSetup() {
		InitAnimation();
		Init();
		int i;
		// c'è il body?        
		if ((body != null) && (body.Length > 0)) {
			if (currentBody != null) {
				for (i = 0; i < currentBody.Count; i++) {
					skr.sharedMaterials[currentBody[i]].mainTexture = body[Random.Range(0, body.Length)];
					// skr.materials[currentBody[i]].SetTexture("_MainTex", body[Random.Range(0, body.Length)]);
				}
			}
		}
		// c'è shirt?
		if ((shirt != null) && (shirt.Length > 0)) {			
			if (currentShirt != null) {
				for (i = 0; i < currentShirt.Count; i++) {
					skr.sharedMaterials[currentShirt[i]].mainTexture = shirt[Random.Range(0, shirt.Length)];
				}
			}			
		}
		// c'è cloth?
		if ((cloth != null) && (cloth.Length > 0)) {
			
			if (currentCloth != null) {
				for (i = 0; i < currentCloth.Count; i++) {
					skr.sharedMaterials[currentCloth[i]].mainTexture = cloth[Random.Range(0, cloth.Length)];
				}
			}			
		}
		// c'è shoes?
		if ((shoes != null) && (shoes.Length > 0)) {
			
			if (currentShoes != null) {
				for (i = 0; i < currentShoes.Count; i++) {
					skr.sharedMaterials[currentShoes[i]].mainTexture = shoes[Random.Range(0, shoes.Length)];
				}
			}			
		}
		// c'è trousers?
		if ((trousers != null) && (trousers.Length > 0)) {
			
			if (currentTrousers != null) {
				for (i = 0; i < currentTrousers.Count; i++) {
					skr.sharedMaterials[currentTrousers[i]].mainTexture = trousers[Random.Range(0, trousers.Length)];
				}
			}			
		}		
        
	}

    public void AnimationOptimizer(bool stop)
    {
        if (stop)
        {
            //anm.Stop();
            CancelInvoke("startAnim");
            CharacterMesh.SetActive(false);        
        }
        else
        {
            CharacterMesh.SetActive(true);
            if (!anm.isPlaying)
            {
                anm.Play();
                InvokeRepeating("startAnim", Random.Range(3f, 4f), 40);
            }
        }

    }
}
