﻿using UnityEngine;

public class PassengersGenerator : MonoBehaviour {

	public GameObject[]	passengersList;
	private float[]		padX = {1.877f, 1.093f, 0.302f, -1.402f, -2.115f};
	private float[]		padY = {0f, 0f, 0f, 0.01f, 0.102f, 0.061f};
	private float padZ = -1.2f;
	public int rows = 19;
	private const float startZ = 0.148f;
	public TextAsset	disp;

    GameGestor gameg;

	void Awake () {
        gameg = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameGestor>();
		InitPassengers();
        //this.gameObject.SetActive(false);
       
	}

	public void InitPassengers() {
        int n = 0;
		int i, k, p;
		float z;
		string[] records;
		string[] chr;
		// Legge la disposizione da file txt
		records = disp.text.Split(new char[] {'\n'});
		z = startZ;
		rows = records.Length;
		Random.seed = 1;
		for (i = 0; i < rows; i++) {
			chr = records[i].Trim().Split(new char[] {','});
			for (k = 1; k < chr.Length; k++) {
                p = (chr[k] != "") ? Random.Range(0, passengersList.Length) : -1;
				if (p != -1) {
                    n++;
					GameObject go = Instantiate(passengersList[p]) as GameObject;
                    go.name = "Passeggero_" + n;
					go.transform.parent = transform;
					go.transform.localPosition = new Vector3(padX[k - 1], padY[p], z);
					CharactersManager cm = go.GetComponent<CharactersManager>();
					cm.RandomSetup(); //randomizza la creazione del passeggero
                    //aggiungo il passeggero alla lista
                    gameg.arrayPasseggeri.Add(cm);                    
				}
			}
			z += padZ;
		}
	}
}

/*
FILA01 [PASS66] [PASS10] 	[PASS77] [PASS50] [PASS12]
FILA02 [PASS74] [PASS51] 	[PASS13] [PASS59]
FILA03 [PASS76] [PASS11] 	[PASS58] [PASS14] [PASS60]
FILA04 [PASS15] 			[PASS40] [PASS01] [PASS02]
FILA05 [PASS56] [PASS57] 	[PASS03] [PASS04]
FILA06 [PASS16] [PASS17] 	[PASS22] [PASS23] [PASS24]
FILA07 [PASS18] [PASS19] 	         [PASS25] [PASS26]
FILA08 [PASS41] [PASS65] 	[PASS28] [PASS69] [PASS49]
FILA09 						[PASS61] [PASS27]
FILA10 [PASS52] [PASS75] 	[PASS21] [PASS31] [PASS62]
FILA11 [PASS46] [PASS20] 	         [PASS29] [PASS43]
FILA12 [PASS68] [PASS73] 	[PASS30] [PASS48] [PASS70]
FILA13 [PASS06] [PASS07] 	         [PASS32] [PASS63]
FILA14 [PASS08] [PASS09] 	[PASS71] [PASS47] [PASS33]
FILA15 [PASS34]	[PASS38]
FILA16 [PASS55] [PASS35] 			 [PASS36] [PASS64]
FILA17 						[PASS42] [PASS53] [PASS54]
FILA18 [PASS67] [PASS27] 					  [PASS39]
FILA19 [PASS44] [PASS72] 	         [PASS05] [PASS45]
*/