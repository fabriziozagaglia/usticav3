﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

    public bool isOpen = false;
    public Vector3 OpenRotation, ClosedRotation;
    public Vector3 target;

    public bool idle = true;

	// Use this for initialization
	void Awake() {
        isOpen = false;
        this.gameObject.transform.localRotation=Quaternion.Euler(ClosedRotation);
        idle = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (!idle)
        {
            if (this.gameObject.transform.localRotation == Quaternion.Euler(target))
            {
                idle = true;
            }
            else
            {
                //lerping
                this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, Quaternion.Euler(target), 3 * Time.deltaTime);
            }
        }
	}

    public void Interact()
    {
        idle = false;
        if (isOpen)
        {
            isOpen = false;
            target = ClosedRotation;
        }
        else
        {
            isOpen = true;
            target = OpenRotation;
        }
    }
}
