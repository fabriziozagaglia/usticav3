﻿using UnityEngine;
using System.Collections;

public class PlayerGestor : MonoBehaviour
{    
    public int id;
    public GameObject Objs;
    public GameObject Player;
    Vector3 StartPosition;
    Quaternion StartRotation;
    public bool isUnlocked;
    [SerializeField] bool NeedCameraResetOnSwitch;
    [SerializeField] public CameraUstica thisCamera;
    
    void Awake()
    {
        Init();
    }

    void FixedUpdate()
    {

    }

    private void Init()
    {
        StartPosition = Player.transform.position;
        StartRotation = Player.transform.rotation;
    }

    public void SetActiveGameObject(bool active)
    {
        if (NeedCameraResetOnSwitch)
        {
            Player.transform.rotation = StartRotation;
            Player.transform.position = StartPosition;
        }
        Player.SetActive(active);        
    }
}
