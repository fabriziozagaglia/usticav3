﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerInteractor : MonoBehaviour {

    RaycastHit hit;
    Ray rayInteraction;
    bool showingGUI = false;
    public GameObject InteractionGUI;

    public float WaitUntilOtherInput;
    float prevTimeInput;
	
	// Update is called once per frame
	void Update () {
        showingGUI = false;
        rayInteraction = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(rayInteraction,out hit, 2,1<<LayerMask.NameToLayer("ObjsInteragibili")))
        {
            showingGUI = true;
            //vedo se il giocatore preme l'input
            if (GetInputInteraction())
            {
                Debug.Log("Interazione con l'oggetto " + hit.collider.gameObject.name);
                switch (hit.collider.gameObject.tag)
                {
                    case "Door":
                            hit.collider.gameObject.GetComponent<DoorScript>().Interact();
                        break;
                    case "Sedile":
                        {

                        }
                        break;
                    default:
                        {
                            
                        }
                        break;
                }
            }
        }
        ShowInteractGui();
    }

    private void ShowInteractGui()
    {
        InteractionGUI.SetActive(showingGUI);
    }

    private bool GetInputInteraction()
    {
        bool result = false;
        if (prevTimeInput > WaitUntilOtherInput)
        {
            prevTimeInput = 0;
            result = (CrossPlatformInputManager.GetAxis("Fire1") > 0);
        }
        else
        {
            prevTimeInput += Time.deltaTime;
        }
        return result;
    }
}
